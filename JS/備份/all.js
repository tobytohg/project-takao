let district = document.querySelector('#district');
let list = document.querySelector('.list');
let pageLine = document.querySelector('.page-line');
let data;
let storage = [];
let cardNum = 0;


// axios 取得 API
axios.get('https://api.kcg.gov.tw/api/service/get/9c8e1450-e833-499c-8320-29b36b7ace5c')
    .then((res) => {
    data = res.data.data.XML_Head.Infos.Info;
})

// 選擇行政區觸發事件
district.addEventListener('change',update,false);



function update(e){

    // 取得Area
    getArea(e);

    // 渲染到畫面上
    let str = '';
};

function getArea(e){
    // 先把 cardNum歸零，不然每次換區都會累積
    cardNum = 0;

    // 因為要用array.push，所以需先將陣列清空
    storage = [];
    // 渲染到畫面上
    let str = '';

    // forEach 去取得陣列參數
    data.forEach((item) => {
        // 用 includes 拿所選區域的資料
        if(item.Add.includes(e.target.value)){
            cardNum += 1;
            storage.push({name: item.Name, area: e.target.value, tel: item.Tel, opentime: item.Opentime, add: item.Add, img: item.Picture1});
            
            
            str += '<li>名字：'+ item.Name +'-地區：'+ e.target.value +
            '-電話：'+ item.Tel +'</li>';
        }
    });

    getPage(Math.ceil((cardNum/8)));
    console.log('Hey'+Math.ceil((cardNum/8)));
    list.innerHTML = str;
}

function getPage(num){
    // 存頁數用
    let pageSum = [];
    let pageStr = '<a href="#" class="prev">prev</a>';
    for(i=0; i<num; i++){
        pageSum.push(i+1);
        pageStr += '<a href="#" class="page" data-num="'+ i +'">'+ (i+1) +'</a>';
    }
    pageStr += '<a href="#" class="next">next</a>';
    pageLine.innerHTML = pageStr;

    // 把每一頁都綁定 EVENT
    bindPage(pageSum);
};

function bindPage(num){
    let page = document.querySelectorAll('.page');
    
    // 用迴圈綁定每個 event
    for(i=0; i<num.length; i++){
        page[i].addEventListener('click',renderPage,false);
    }
}

function renderPage(e){
    console.log('yeah!'+e.target.dataset.num);

}