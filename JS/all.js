let district = document.querySelector('#district');
let list = document.querySelector('.list');
let pageLine = document.querySelector('.page-line');
let districtBlock = document.querySelectorAll('.district-block');
let data;
let nowPage;
let storage = [];
let cardNum = 0;
let page;
let pageAll = 0;
let itemSum = 0;
// axios 取得 API
axios.get('https://api.kcg.gov.tw/api/service/get/9c8e1450-e833-499c-8320-29b36b7ace5c')
.then((res) => {
    data = res.data.data.XML_Head.Infos.Info;

    // 選擇行政區觸發事件
    district.addEventListener('change',getArea,false);
    // forEach 綁定 district-line 的每個district-block
    districtBlock.forEach((item) => {
        item.addEventListener('click',getArea,false);
    });
}).catch((fail) => {
    console.log(fail);
    console.log('failure');
})

function test(e){
    console.log(e.target.innerText)
}

function getArea(e){
    // 先把 cardNum歸零，不然每次換區都會累積
    cardNum = 0;
    // 因為要用array.push，所以需先將陣列清空
    storage = [];
    // 渲染到畫面上
    let str = '';
    // forEach 去取得陣列參數
    data.forEach((item) => {
        // 用 includes 拿所選區域的資料
        if(item.Add.includes(e.target.value) || item.Add.includes(e.target.innerText)){
            cardNum += 1;
            storage.push({
                name: item.Name,
                area: e.target.value || e.target.innerText,
                tel: item.Tel,
                opentime: item.Opentime,
                add: item.Add,
                img: item.Picture1,
                fee: (item.Ticketinfo === '' || item.Ticketinfo === '免費入園') ? '免費參觀' : ''
            });
            itemSum += 1;
        }
    });
    itemSum = 0;
    // Math.ceil 回傳大於等於所給數字的最小整數。
    // 一頁八個資訊，所以除以八
    getPage(Math.ceil((cardNum/8)));
    updatePage();
}

function getPage(num){
    // 存頁數用
    let pageSum = [];
    let pageStr = '<a  class="prev">< prev</a>';
    // 迴圈去創造頁數
    for(i=0; i<num; i++){
        pageSum.push(i+1);
        pageStr += '<a  class="page" data-num="'+ i +'">'+ (i+1) +'</a>';
    }
    pageStr += '<a class="next">next ></a>';
    // render 到 HTML
    pageLine.innerHTML = pageStr;

    // 宣告 prev 跟 next 變數並綁定事件
    let prev = document.querySelector('.prev');
    let next = document.querySelector('.next');
    prev.addEventListener('click',updatePage,false);
    next.addEventListener('click',updatePage,false);

    // 把每一頁都綁定 EVENT
    bindPage(pageSum);
    pageAll = pageSum;
};

function bindPage(num){
    // 拿取所有 page class
    page = document.querySelectorAll('.page');
    
    // 用迴圈綁定每個 event
    for(i=0; i<num.length; i++){
        page[i].addEventListener('click',updatePage,false);
    }
}

function updatePage(e){
    let str = '';
    // let nowPage;

    // 一開始會是0
    // 點擊HTML的target.num拿到的會是「字串」，所以要用parseInt轉成數字
    if(e == null){
        nowPage = 0;
    }else if(e.target.getAttribute('class') == 'next'){
        if(nowPage < (pageAll.length-1)){
            nowPage += 1;
        }
    }else if(e.target.getAttribute('class') == 'prev'){
        if(nowPage > 0){
            arrowStr = e.target.getAttribute('class');
            nowPage -= 1;
        }
    }else{
        nowPage =  parseInt(e.target.dataset.num);
    }

    // if(e == null){
    //     nowPage = 0;
    // }else{
    //     nowPage =  parseInt(e.target.dataset.num);
    // }

    // 第一頁的話
    if(nowPage == 0){
        for(i=0; i<8; i++){
            // 有可能連八個都不到，要是不判斷會出錯
            if(storage[i] == undefined){
                console.log('沒東西');
            }else{
            str += `
            <div class="dis-card">
                <div class="top-card">
                    <div class="name-line">
                        <h3 class="location-name">${storage[i].name}</h3>
                        <span class="area-name">${storage[i].area}</span>
                    </div>
                </div>
                <div class="bottom-card">
                    <div class="left-area">
                        <p class="location-time">${storage[i].opentime}</p>
                        <p class="location-add">${storage[i].add}</p>
                        <p class="location-tel">${storage[i].tel}</p>
                    </div>
                    <div class="right-area">
                        <p class="fee">${storage[i].fee}</p>
                    </div>
                </div>
            </div>`
            }
        }
    }else{
        for(i=(8*nowPage); i<(8*(nowPage+1)); i++){
            // 可能不滿八個，用來判斷還有沒有item
            if(storage[i] == undefined){
                console.log('沒東西')
            }else{
            // 判斷有無門票
                str += `
                    <div class="dis-card">
                        <div class="top-card">
                            <div class="name-line">
                                <h3 class="location-name">${storage[i].name}</h3>
                                <span class="area-name">${storage[i].area}</span>
                            </div>
                        </div>
                        <div class="bottom-card">
                            <div class="left-area">
                                <p class="location-time">${storage[i].opentime}</p>
                                <p class="location-add">${storage[i].add}</p>
                                <p class="location-tel">${storage[i].tel}</p>
                            </div>
                            <div class="right-area">
                                <p class="fee">${storage[i].fee}</p>
                            </div>
                        </div>
                    </div>`
            }
        }
    }
    let currentArea = document.querySelector('.current-dis');
    currentArea.innerHTML = storage[0].area;
    // list.innerHTML = str;
    document.querySelector('.district-list').innerHTML = str;
    currentPage();
}

function currentPage(){
    // 過濾找出目前的 page
    for(i=0; i<pageAll.length; i++){
        // 先把全部 page 的 class 清回只有class
        page[i].setAttribute('class','page');

        // 再把目前的 page 加上 class
        if(parseInt(page[i].dataset.num) == nowPage){
            page[i].classList.add('Current-Page');
        }
        // 第一頁特別加上 first-page 的 class
        if(parseInt(page[i].dataset.num) === 0){
            page[i].classList.add('first-page');
        }
    }
}